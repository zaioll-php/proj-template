<?php

declare(strict_types=1);

namespace Zaioll\Shared\Application\Service;

use Exception;
use Zaioll\Shared\Application\Service\NoTransactionalSession;
use Zaioll\Shared\Application\Service\NoTransactionalApplicationServiceException;

final class NoTransactionalApplicationServiceDecorator extends ApplicationServiceDecorator
{
    /**
     * @param ApplicationServiceInterface $service
     * @param NoTransactionalSession $session
     */
    public function __construct(ApplicationServiceInterface $service, NoTransactionalSession $session)
    {
        parent::__construct($service, $session);
    }

    /**
     * @inheritDoc
     */
    public static function isTransactional(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    protected function thorwException(string $message, int $code, Exception $ex)
    {
        throw new NoTransactionalApplicationServiceException($message, $code, $ex);
    }
}

<?php

declare(strict_types=1);

namespace Zaioll\Shared\Application\Service;

use Exception;
use Zaioll\Shared\Application\Service\TransactionalSession;
use Zaioll\Shared\Application\Service\TransactionalApplicationServiceException;

final class TransactionalApplicationServiceDecorator extends ApplicationServiceDecorator
{
    /**
     * @param ApplicationServiceInterface $service
     * @param TransactionalSession $session
     */
    public function __construct(ApplicationServiceInterface $service, TransactionalSession $session)
    {
        parent::__construct($service, $session);
    }

    /**
     * @inheritDoc
     */
    public static function isTransactional(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    protected function thorwException(string $message, int $code, Exception $ex)
    {
        throw new TransactionalApplicationServiceException($message, $code, $ex);
    }
}

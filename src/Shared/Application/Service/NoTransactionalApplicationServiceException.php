<?php

declare(strict_types=1);

namespace Zaioll\Shared\Application\Service;

use Zaioll\Shared\Application\Service\ApplicationServiceException;

class NoTransactionalApplicationServiceException extends ApplicationServiceException
{
}

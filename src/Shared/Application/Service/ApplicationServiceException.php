<?php

declare(strict_types=1);

namespace Zaioll\Shared\Application\Service;

use Zaioll\Shared\Domain\Model\DomainException;

class ApplicationServiceException extends DomainException
{
}

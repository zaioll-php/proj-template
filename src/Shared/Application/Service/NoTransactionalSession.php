<?php

declare(strict_types=1);

namespace Zaioll\Shared\Application\Service;

use Exception;
use Zaioll\Shared\Application\Service\Session;

interface NoTransactionalSession extends Session
{
}

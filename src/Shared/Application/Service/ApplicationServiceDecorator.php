<?php

declare(strict_types=1);

namespace Zaioll\Shared\Application\Service;

use Exception;
use Zaioll\Shared\Domain\Bus\Request;

abstract class ApplicationServiceDecorator implements ApplicationServiceInterface
{
    /**
     * @var ApplicationServiceInterface
     */
    protected $service;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @param ApplicationServiceInterface $service
     * @param Session $session
     */
    public function __construct(ApplicationServiceInterface $service, Session $session)
    {
        $this->service = $service;
        $this->session = $session;
    }


    /** @throws ApplicationServiceException */
    public function execute(Request $request)
    {
        $operation = function () use ($request) {
            return $this->service->execute($request);
        };

        try {
            $result = $this->session->executeOperation($operation);
        } catch (Exception $ex) {
            $this->thorwException($ex->getMessage(), (int)$ex->getCode(), $ex);
        }

        return $result;
    }

    /**
     * @param string $message
     * @param integer $code
     * @param Exception $ex
     *
     * @return void
     */
    abstract protected function thorwException(string $message, int $code, Exception $ex);
}

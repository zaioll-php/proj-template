<?php

declare(strict_types=1);

namespace Zaioll\Shared\Application\InputRequest;

interface InputData
{
    /**
     * @return object
     */
    public function getRequest();

    /**
     * @return bool
     */
    public function isValid(): bool;

    /**
     * @return mixed[]
     */
    public function getErrors(): array;
}

<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\Criteria;

use Zaioll\Shared\Domain\ValueObject\StringValueObject;

final class FilterValue extends StringValueObject
{
    protected static function defineConstraints(): array
    {
        return [];
    }
}

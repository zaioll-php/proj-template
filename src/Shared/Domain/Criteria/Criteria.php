<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\Criteria;

use Zaioll\Shared\Domain\Criteria\Order;
use Zaioll\Shared\Domain\Criteria\Filters;

final class Criteria
{
    private $filters;

    private $order;

    private $offset;

    private $limit;

    public function __construct(
        Filters $filters,
        Order $order,
        ?int $offset,
        ?int $limit
    ) {
    }

    public function hasFilters(): bool
    {
        return $this->filters->count() > 0;
    }

    public function hasOrder(): bool
    {
        return ! $this->order->isNone();
    }

    public function plainFilters(): array
    {
        return $this->filters->filters();
    }

    public function filters(): Filters
    {
        return $this->filters;
    }

    public function order(): Order
    {
        return $this->order;
    }

    public function offset(): ?int
    {
        return $this->offset;
    }

    public function limit(): ?int
    {
        return $this->limit;
    }

    public function serialize(): string
    {
        return sprintf(
            '%s~~%s~~%s~~%s',
            $this->filters->serialize(),
            $this->order->serialize(),
            $this->offset,
            $this->limit
        );
    }
}

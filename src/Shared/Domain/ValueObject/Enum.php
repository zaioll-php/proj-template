<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\ValueObject;

use Zaioll\Shared\Infrastructure\Helper\ArrayHelper;
use Zaioll\Shared\Infrastructure\Helper\StringHelper;
use ReflectionClass;

use function Lambdish\Phunctional\reindex;

abstract class Enum
{
    /**
     * @var array
     */
    protected static $cache = [];

    public function fromString(string $valor): Enum
    {
        return new static($valor);
    }

    public function __construct(string $valor)
    {
        $this->ensuresConstraints($valor);
    }

    public static function values(): array
    {
        $class = static::class;

        if (! isset(self::$cache[$class])) {
            $reflected           = new ReflectionClass($class);
            self::$cache[$class] = reindex(self::keysFormatter(), $reflected->getConstants());
        }

        return self::$cache[$class];
    }

    /**
     * @return \Callable
     */
    private static function keysFormatter()
    {
        return static function ($unused, string $key) {
            return (string) StringHelper::toCamelCase(strtolower($key));
        };
    }

    public function value()
    {
        return $this->value;
    }

    public function equals(Enum $other): bool
    {
        return $other == $this;
    }

    public function __toString(): string
    {
        return (string) $this->value();
    }

    private function ensuresConstraints($value): void
    {
        if (! ArrayHelper::isIn($value, static::values(), true)) {
            $this->throwExceptionForInvalidValue($value);
        }
    }

    abstract protected function throwExceptionForInvalidValue($value);
}

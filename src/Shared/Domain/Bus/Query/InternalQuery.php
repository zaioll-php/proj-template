<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\Bus\Query;

use Zaioll\Shared\Domain\Bus\Query\Query;

interface InternalQuery extends Query
{
}

<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\Bus\Command;

use Zaioll\Shared\Domain\Bus\Request;

interface Command extends Request
{
}

<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\Bus;

interface MessageBusEnvelopeInstantiator
{
    /**
     * @param object           $message
     * @param StampInterface[] $stamps
     *
     * @return object Wrapped envelope
     */
    public function createEnvelope($message, array $stamps = []);
}

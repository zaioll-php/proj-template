<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\Bus\Event;

use Zaioll\Shared\Domain\Bus\Event\DomainEvent;

interface EventPublisher
{
    public function publish(DomainEvent ...$events): void;
}

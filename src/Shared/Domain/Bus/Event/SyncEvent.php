<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\Bus\Event;

use Zaioll\Shared\Domain\Bus\Event\Event;

interface SyncEvent extends Event
{
}

<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\Model;

use Psr\SimpleCache\CacheInterface;

interface CacheInMemory extends CacheInterface
{
    public function prefixed(string $key): string;

    /**
     * @return array|false
     */
    public function getAll();
}

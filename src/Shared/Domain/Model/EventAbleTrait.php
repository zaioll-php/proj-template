<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\Model;

use Zaioll\Shared\Domain\Bus\Event\DomainEvent;
use Zaioll\Shared\Infrastructure\Helper\ArrayHelper;

trait EventAbleTrait
{
    /**
     * @var array
     */
    private $domainEvents = [];

    /**
     * @return DomainEvent[]
     */
    final public function dropEvents(): array
    {
        $domainEvents       = $this->domainEvents;
        $this->domainEvents = [];

        return $domainEvents;
    }

    final protected function collectEvent(DomainEvent $domainEvent): void
    {
        $this->domainEvents[] = $domainEvent;
    }

    final protected function countEvents(): int
    {
        return ArrayHelper::count($this->domainEvents);
    }
}

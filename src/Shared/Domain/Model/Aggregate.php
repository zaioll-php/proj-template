<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\Model;

use Zaioll\Shared\Domain\Model\Entity;

interface Aggregate extends Entity
{
}

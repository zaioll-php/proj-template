<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\Model;

interface Entity
{
    /**
     * @param array $state
     *
     * @return self
     */
    public static function fromState(array $state): self;

    /**
     * @return array mappedData
     */
    public function getState(): array;
}

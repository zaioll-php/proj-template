<?php

namespace Zaioll\Shared\Infrastructure\InputRequest;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Throwable;

final class SymfonyBadRequestHttpException extends BadRequestHttpException
{
    public function __construct(array $errors, $code = 0, Throwable $previous = null)
    {
        $message = "Bad Request:\n";
        foreach ($errors as $error) {
            $source = $error['source']['pointer'] ?? '';
            $detail = $error['detail'];

            $message .= "[ source: '{$source}', details: '{$detail}' ]\n";
        }

        parent::__construct($message, $previous, $code);
    }

    /**
     * @return mixed[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}

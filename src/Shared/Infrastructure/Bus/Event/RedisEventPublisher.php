<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Bus\Event;

use Zaioll\Shared\Domain\Bus\Event\DomainEvent;
use Zaioll\Shared\Domain\Bus\Event\EventPublisher;
use Zaioll\Shared\Infrastructure\Helper\ArrayHelper;
use Symfony\Component\Messenger\MessageBusInterface;

final class RedisEventPublisher implements EventPublisher
{
    protected $eventBus;

    public function __construct(MessageBusInterface $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    public function publish(DomainEvent ...$events): void
    {
        if (ArrayHelper::count($events) === 0) {
            return;
        }
        foreach ($events as $event) {
            $this->eventBus->dispatch($event);
        }
    }
}

<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Service;

use Zaioll\Shared\Application\Service\TransactionalSession;
use Zaioll\Shared\Infrastructure\Component\ORM\EntityManagerInterface;

final class ORMTransactionalSession implements TransactionalSession
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function executeOperation(callable $operation)
    {
        return $this->entityManager->transactional($operation);
    }
}

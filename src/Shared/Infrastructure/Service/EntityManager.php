<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Service;

use Zaioll\Shared\Infrastructure\Component\ORM\EntityManagerInterface;
use Doctrine\ORM\Decorator\EntityManagerDecorator as ORMEntityManager;

final class EntityManager extends ORMEntityManager implements EntityManagerInterface
{
}

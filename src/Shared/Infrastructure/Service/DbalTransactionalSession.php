<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Service;

use Zaioll\Shared\Infrastructure\Component\Db\Connection;
use Zaioll\Shared\Application\Service\TransactionalSession;

final class DbalTransactionalSession implements TransactionalSession
{
    private $dbal;

    public function __construct(Connection $dbal)
    {
        $this->dbal = $dbal;
    }

    public function executeOperation(callable $operation)
    {
        return $this->dbal->transactional($operation);
    }
}

<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Db;

use Zaioll\Shared\Application\Service\Storage;

interface DbStorage extends Storage
{
}

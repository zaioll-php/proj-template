<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Db;

use Doctrine\DBAL\Cache\QueryCacheProfile;
use Symfony\Contracts\Cache\CacheInterface;
use Zaioll\Shared\Infrastructure\Helper\ArrayHelper;
use Symfony\Contracts\Cache\TagAwareCacheInterface;
use Zaioll\Shared\Infrastructure\Component\Db\DbStorage;
use Zaioll\Shared\Infrastructure\Component\Db\Connection;
use Zaioll\Shared\Infrastructure\Component\ORM\DBALConnectionAdapter;
use Symfony\Contracts\Cache\ItemInterface;

class SqlDbStorage implements DbStorage
{
    /**
     * @var DBALConnectionAdapter
     */
    private $main;

    private $cache;

    public function __construct(
        Connection $connection,
        TagAwareCacheInterface $cacheChainGlobal
    ) {
        $this->main  = $connection;
        $this->cache = $cacheChainGlobal;

        $this->main->getConfiguration()->setResultCache($cacheChainGlobal);
    }

    /**
     * @param string $sql
     * @param array $params
     *
     * @return \Doctrine\DBAL\Result
     */
    public function execute(string $sql, array $params = [])
    {
        $command = $this->getConnection()->prepare($sql);

        $qtdParams  = ArrayHelper::count($params);
        if ($qtdParams > 0) {
            foreach ($params as $index => $param) {
                $command->bindValue($index + 1, $param);
            }
        }
        return $command->execute();
    }

    /**
     * @param string $table
     * @param array $data
     *
     * @return int|false
     */
    public function save(string $table, array $data, array $types)
    {
        $isUpdate = $data['ID'] ?? false;
        if ($isUpdate === false) {
            return $this->legacyInsert($table, $data);
        }
        $id = $data['ID'];
        unset($data['ID']);

        $this->update($table, $data, ['ID' => $id], $types);

        return $id;
    }

    /**
     * @param string $table
     * @param array $data
     *
     * @return bool
     * @throws Exception
     */
    public function update(string $table, array $data, array $criteria, array $types)
    {
        $result = $this->getConnection()->update($table, $data, $criteria, $types);

        return $result > 0;
    }

    /**
     * @param string $tabela
     * @param array $dados
     * @param array $campos
     *
     * @return int Qtd registros afetados
     */
    public function legacyInsert(string $tabela, array $dados, array $campos = [])
    {
        $sqlcampos  = '';
        $sqldados   = '';
        $data       = [];
        if (ArrayHelper::count($dados) > 0) {
            $fields     = ArrayHelper::keys($dados);
            $data       = ArrayHelper::values($dados);

            $sqlcampos  = ArrayHelper::implode('`, `', $fields);
            $sqldados   = ':' . ArrayHelper::implode(', :', $fields);

            $sqlcampos = "`{$sqlcampos}`";
        }

        $sqlcamposcomandos  = '';
        $sqldadoscomandos   = '';
        if (ArrayHelper::count($campos) > 0) {
            $camposcomando  = ArrayHelper::keys($campos);
            $dadoscomando   = ArrayHelper::values($campos);
            if ($sqlcampos !== '') {
                $sqlcamposcomandos  .= ', ';
                $sqldadoscomandos   .= ', ';
            }
            $sqlcamposcomandos = ArrayHelper::implode(', ', $camposcomando);
            $sqldadoscomandos  = ArrayHelper::implode(', ', $dadoscomando);
        }

        /*
        $nomebdatl = '';//self::getBDname().".";
        if (strpos($tabela, "app_000500") !== false || strpos($tabela, "app_allids") !== false || strpos($tabela, "apps_informacoes") !== false || strpos($tabela, "apps_ifood") !== false){
            $nomebdatl = '';
        } elseif (strpos($tabela, ".") !== false) {
            $nomebdatl = '';
        } elseif (strpos($tabela, $nomebdatl) !== false) {
            $nomebdatl = '';
        }
        */

        $sql    = <<<SQL
            INSERT INTO {$tabela}
                ({$sqlcampos}{$sqlcamposcomandos}) VALUES ({$sqldados}{$sqldadoscomandos})
SQL
        ;

        $command = $this->getConnection()->prepare($sql);
        $qtdParams  = ArrayHelper::count($data);
        for ($i = 0; $i < $qtdParams; $i += 1) {
            $command->bindValue(":{$fields[$i]}", $data[$i]);
        }
        $command->execute();

        return $this->getConnection()->lastInsertId();
    }

    /**
     * @param string $query
     * @param array $params
     * @param string $cacheKey
     * @param array $types
     * @param int $lifetime
     *
     * @return array
     */
    public function fetchAllAssociativeQueryCache(
        string $query,
        array $params,
        string $cacheKey,
        int $lifetime = 0,
        $types = []
    ) {
        $instance = $this;
        $result = $this->getCache()->get(
            $cacheKey,
            function () use ($query, $params, $lifetime, $cacheKey, $instance, $types) {
                /** @var self $instance */
                $cacheProfile = $instance->getCacheProfile($lifetime, $cacheKey);

                $result = $instance->getConnection()->executeQuery($query, $params, $types, $cacheProfile);
                if ($result->columnCount() === 0) {
                    return [];
                }

                return $result->fetchAllAssociative();
            }
        );

        return $result;
    }

    private function getCacheProfile(int $lifetime, ?string $cacheKey = null)
    {
        return new QueryCacheProfile($lifetime, $cacheKey, $this->cache);
    }

    /**
     * @return DBALConnectionAdapter
     */
    public function getConnection(): Connection
    {
        return $this->main;
    }

    /**
     * @return CacheInterface
     */
    public function getCache(): CacheInterface
    {
        return $this->cache;
    }

    /**
     * @return string
     */
    public function getDbName(): string
    {
        return $this->getConnection()->getDatabase();
    }

    /**
     * @param array $params
     *
     * @return string
     */
    public function positionalArgsBinds(array $params): string
    {
        $binds = '';
        foreach ($params as $param) {
            if (strlen($binds) === 0) {
                $binds .= '?';
                continue;
            }
            $binds .= ',?';
        }
        return "({$binds})";
    }
}

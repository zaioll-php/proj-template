<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Db;

class DbStringConnectionParser
{
    public $dsnParser;

    protected $dsn;

    protected $host;

    protected $porta;

    protected $dbname;

    protected $driver;

    protected $autenticacao;

    protected $stringConexaoUrl;

    private function __construct($dsn)
    {
        $this->dsn = $dsn;
    }

    public static function instance($dbStringConnection, $dsnParser)
    {
        $instancia = new static($dbStringConnection);
        $instancia->dsnParser = $dsnParser;

        return $instancia;
    }

    private function stringConexaoUrl()
    {
        if (is_null($this->stringConexaoUrl)) {
            $this->stringConexaoUrl = self::isUrl($this->dsn);
        }
        return $this->stringConexaoUrl;
    }

    private static function isUrl($stringConexao)
    {
        $driver = self::parseDriver($stringConexao);
        return (is_null($driver) || empty($driver)) ? true : false;
    }

    /**
     * @param string $dsn
     * @return array
     */
    public static function parse($stringConexao)
    {
        $dsn = [];
        if (! self::isUrl($stringConexao)) {
            $dsn['driver'] = self::parseDriver($stringConexao);
            $dsn['dbname'] = self::parseDbname($stringConexao);
            $dsn['host']   = self::parseHost($stringConexao);
            $dsn['porta']  = self::parsePorta($stringConexao);
            $dsn['autenticacao'] = self::parseAutenticacao($stringConexao);
        }
        return $dsn;
    }

    private static function parseAutenticacao($dsn)
    {
        return [
            'username' => self::parseUsuario($dsn),
            'password' => self::parseSenha($dsn),
        ];
    }

    private static function parseSenha($dsn)
    {
        preg_match('/(?<=password\=)[^=\:\s\;]+/i', $dsn, $matches);
        return (isset($matches[0])) ? $matches[0] : '';
    }

    private static function parseUsuario($dsn)
    {
        preg_match('/(?<=user\=)[\w\-._]+/i', $dsn, $matches);
        return (isset($matches[0])) ? $matches[0] : '';
    }

    /**
     * @param string $dsn
     *
     * @return string
     */
    private static function parseDriver($dsn)
    {
        if ($dsn === null) {
            return '';
        }
        preg_match('/\w+(?=:(host|port|dbname|senha|user)\=)/i', $dsn, $matches);

        return $matches[0] ?? '';
    }

    private static function parseDbname($dsn)
    {
        preg_match('/(?<=dbname\=)[\w\-._]+/i', $dsn, $matches);
        return (isset($matches[0])) ? $matches[0] : '';
    }

    public function dsnParser()
    {
        return $this->dsnParser;
    }

    private static function parsePorta($dsn)
    {
        preg_match('/(?<=port\=)\d+/i', $dsn, $matches);
        return (isset($matches[0])) ? $matches[0] : '';
    }

    private static function parseHost($dsn)
    {
        preg_match('/(?<=host\=)[\w\-._]+/i', $dsn, $matches);
        return (isset($matches[0])) ? $matches[0] : '';
    }

    public function dbname()
    {
        if (is_null($this->dbname)) {
            $this->dbname = ($this->stringConexaoUrl())
                ? $this->dsnParser()->getDatabase()
                : self::parseDbname($this->dsn);
        }

        return $this->dbname;
    }

    public function driver()
    {
        if (is_null($this->driver)) {
            $dsnParser    = $this->dsnParser->className();
            $this->driver = ($this->stringConexaoUrl())
                ? $dsnParser::parseProtocol($this->dsn)
                : self::parseDriver($this->dsn);
        }
        return $this->driver;
    }

    /**
     * @return string
     */
    public function usuario(): string
    {
        if (is_null($this->autenticacao)) {
            $this->autenticacao = ($this->stringConexaoUrl())
                ? $this->dsnParser()->getAuthentication()
                : self::parseAutenticacao($this->dsn);
        }
        return $this->autenticacao['username'] ?? '';
    }

    /**
     * @return string
     */
    public function senha(): string
    {
        if (is_null($this->autenticacao)) {
            $this->autenticacao = ($this->stringConexaoUrl())
                ? $this->dsnParser()->getAuthentication()
                : self::parseAutenticacao($this->dsn);
        }
        return $this->autenticacao['password'] ?? '';
    }

    public function porta()
    {
        if (is_null($this->porta)) {
            $this->porta = ($this->stringConexaoUrl())
                ? $this->dsnParser()->getFirstPort()
                : self::parsePorta($this->dsn);
        }
        return $this->porta;
    }

    public function host()
    {
        if (is_null($this->host)) {
            $this->host = ($this->stringConexaoUrl())
                ? $this->dsnParser()->getFirstHost()
                : self::parseHost($this->dsn);
        }
        return $this->host;
    }
}

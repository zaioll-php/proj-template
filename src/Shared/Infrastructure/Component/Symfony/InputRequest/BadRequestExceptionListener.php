<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Symfony\InputRequest;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Zaioll\Shared\Infrastructure\InputRequest\BadRequest;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class BadRequestExceptionListener implements EventSubscriberInterface
{
    /**
     * @return array<string, array<int|string>>
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => ['onKernelException', 0],
        ];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        if ($exception instanceof BadRequest) {
            $event->setResponse(
                new JsonResponse(
                    ['errors' => $exception->getErrors()],
                    Response::HTTP_BAD_REQUEST
                )
            );
        }
    }
}

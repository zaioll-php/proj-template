<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Symfony;

interface InternalServerError
{
    public function getMessage();
}

<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Symfony;

use Zaioll\Shared\Domain\Bus\MessageBusEnvelopeInstantiator;
use Symfony\Component\Messenger\Envelope;

final class SymfonyEnvelopeInstantiator implements MessageBusEnvelopeInstantiator
{
    /**
     * @inheritDoc
     */
    public function createEnvelope($message, array $stamps = [])
    {
        return new Envelope($message, $stamps);
    }
}

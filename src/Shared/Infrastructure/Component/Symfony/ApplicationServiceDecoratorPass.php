<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Symfony;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Zaioll\Shared\Application\Service\ApplicationServiceInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Zaioll\Shared\Application\Service\TransactionalApplicationServiceDecorator;
use Zaioll\Shared\Application\Service\NoTransactionalApplicationServiceDecorator;
use ReflectionClass;

final class ApplicationServiceDecoratorPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (
            ! $container->has(TransactionalApplicationServiceDecorator::class)
            && ! $container->has(NoTransactionalApplicationServiceDecorator::class)
        ) {
            return;
        }

        $taggedServices = $container->findTaggedServiceIds('application_service');
        foreach (array_keys($taggedServices) as $service) {
            if ($service === TransactionalApplicationServiceDecorator::class) {
                continue;
            }

            if ($service === NoTransactionalApplicationServiceDecorator::class) {
                continue;
            }

            [$alias, $argument] = $this->generateAliasName($service, '');
            $type = NoTransactionalApplicationServiceDecorator::class;
            if ($service::isTransactional()) {
                [$alias, $argument] = $this->generateAliasName($service, '_transactional');
                $type = TransactionalApplicationServiceDecorator::class;
            }
            // Add the new decorated service.
            $container
                ->register($alias, $type)
                ->setDecoratedService($service)
                ->setPublic(false)
                ->setAutowired(true)
                ->setAutoconfigured(true);
            // Add the named autowiring alias for wiring with argument
            $container->setAlias(ApplicationServiceInterface::class . " $$argument", $alias);
        }
    }

    /**
     * @return string[]
     */
    private function generateAliasName(string $serviceName, string $type): array
    {
        if (str_contains($serviceName, '\\')) {
            $parts     = explode('\\', $serviceName);
            $className = end($parts);
        } else {
            $className = $serviceName;
        }
        $alias    = strtolower(preg_replace('/[A-Z]/', '_\\0', lcfirst($className)));
        $argument = lcfirst($className);
        return [
            "{$alias}{$type}_decorated",
            $argument
        ];
    }
}

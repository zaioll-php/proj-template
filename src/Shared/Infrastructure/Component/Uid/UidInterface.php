<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Uid;

interface UidInterface
{
    /**
     * Returns the identifier as a RFC4122 case insensitive string.
     *
     * @return string
     */
    public function toRfc4122(): string;

    /**
     * Returns whether the argument is an AbstractUid and contains the same value as the current instance.
     *
     * @return bool
     */
    public function equals(?object $other): bool;
}

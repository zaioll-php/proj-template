<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Uid;

use Ramsey\Uuid\UuidInterface;
use Zaioll\Shared\Infrastructure\Component\Uid\Uuid;

interface UuidInterfaceAdapter extends UuidInterface, Uuid
{
}

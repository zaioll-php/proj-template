<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Uid;

use Zaioll\Shared\Infrastructure\Component\Uid\UidInterface;
use Zaioll\Shared\Infrastructure\Component\Uid\UuidInterfaceAdapter;

interface Uuid extends UidInterface
{
    public static function v1(): UuidInterfaceAdapter;

    public static function v2();

    public static function v3($namespace, string $name): UuidInterfaceAdapter;

    public static function v4(): self;

    public static function v5($namespace, string $name): UuidInterfaceAdapter;

    public static function v6();
}

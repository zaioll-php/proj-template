<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Uid;

use Ramsey\Uuid\Uuid as ExternalUuidLib;
use Zaioll\Shared\Infrastructure\Component\Uid\UuidInterfaceAdapter;

class UuidAdapter extends ExternalUuidLib
{
    public static function v1()
    {
        return self::uuid1();
    }

    public static function v2()
    {
        //return new self();
    }

    public static function v3($namespace, string $name)
    {
        return self::uuid3($namespace, $name);
    }

    public static function v4()
    {
        return self::uuid4();
    }

    public static function v5($namespace, string $name)
    {
        return self::uuid5($namespace, $name);
    }

    public static function v6()
    {
    }

    public function toRfc4122(): string
    {
        // a lib externa atual já implementa
        return (string) $this;
    }
}

<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Collection;

use Zaioll\Shared\Infrastructure\Helper\ArrayHelper;

class LazyCollection
{
    private $elements;

    private $offset;

    private $chunkSize;

    private $filters;

    private $qtdElements;

    public function __construct(array $elements, int $chunkSize, array $filters = [])
    {
        $this->offset       = 0;
        $this->elements     = $elements;
        $this->chunkSize    = $chunkSize;
        $this->filters      = $filters;
    }

    /**
     * @param array $elements
     * @param int $chunkSize
     * @param array $filters
     *
     * @return static
     *
     * @see ArrayHelper::filter();
     */
    public static function create(array $elements, int $chunkSize = 1, array $filters = []): self
    {
        return new static($elements, $chunkSize, $filters);
    }

    /**
     * @param int $size
     *
     * @return self
     */
    public function setChunkSize(int $size): self
    {
        $this->chunkSize = $size;

        return $this;
    }

    private function getQtdElements(): int
    {
        if ($this->qtdElements === null) {
            $this->qtdElements = ArrayHelper::count($this->elements);
        }
        return $this->qtdElements;
    }

    /**
     * @return array
     */
    public function getNextChunk(): array
    {
        $chunk              = [];
        $qtdAdded           = 0;
        $offset             = $this->offset;
        $qtd                = $this->getQtdElements();
        $hasFilters         = ArrayHelper::count($this->filters) > 0;
        for ($i = $offset; $i < $qtd && $i - $offset < $this->chunkSize; $i += 1) {
            if ($qtdAdded === $this->chunkSize - 1) {
                $this->offset = $i + 1;
            }
            $qtdAdded += 1;
            if ($hasFilters) {
                $chunk[] = ArrayHelper::filter($this->elements[$i], $this->filters);
                continue;
            }
            $chunk[] = $this->elements[$i];
        }
        return $chunk;
    }

    public function qtdChunks(): int
    {
        $qtdElements = $this->getQtdElements();
        $chunkSize   = $this->chunkSize;

        $count = 0;
        while ($qtdElements >= $chunkSize) {
            $count          += 1;
            $qtdElements    -= $chunkSize;
        }
        return $count;
    }

    /**
     * Versão string de `$this->getNextChunk()`.
     *
     * @see MerchantIdsCollection::getNextChunk()
     * @return string
     */
    public function getNextChunkString(): string
    {
        return ArrayHelper::implode(',', $this->getNextChunk());
    }

    /**
     * Versão string de `$this->getNextChunk()`.
     *
     * @see MerchantIdsCollection::getNextChunk()
     * @return string
     */
    public function getNextChunkJson(): string
    {
        return ArrayHelper::toJson($this->getNextChunk(), JSON_HEX_QUOT);
    }
}

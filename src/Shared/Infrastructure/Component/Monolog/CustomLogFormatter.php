<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Monolog;

use Monolog\Formatter\FormatterInterface;

class CustomLogFormatter implements FormatterInterface
{
    /**
     * @inheritDoc
     */
    public function format(array $record)
    {
        return $record;
    }

    /**
     * @inheritDoc
     */
    public function formatBatch(array $records)
    {
        return $records;
    }
}

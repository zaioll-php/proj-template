<?php

namespace Zaioll\Shared\Infrastructure\Component\Exception;

use Exception;

class InvalidArgumentException extends Exception
{
    public function getName(): string
    {
        return 'Invalid Argument';
    }
}

<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Redis;

use Generator;
use Zaioll\Shared\Infrastructure\Component\Redis\Redis;

class RedisClientMock
{
    private $messages;

    public function __construct()
    {
        $this->messages = [];
    }

    /**
     * @inheritDoc
     */
    public function publish(string $channel, string $message)
    {
        $this->messages["{$channel}"][] = $message;
    }

    /**
     * @inheritDoc
     */
    public function scan(string $key, int $count): Generator
    {
    }

    /**
     * @param string|null $channel
     *
     * @return array
     */
    public function getMessages(?string $channel = null): array
    {
        return $channel === null ? $this->messages : $this->messages["{$channel}"] ?? [];
    }

    /**
     * @inheritDoc
     */
    public function scanToKeys($key, int $count = 12000)
    {
    }

    public function prefixed(string $key): string
    {
    }

    public function keyExists(string $key): bool
    {
    }

    public function get($key)
    {
    }

    public function set($key, $value, $timeout = null): bool
    {
    }

    public function del(...$keys): int
    {
    }
}

<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Redis;

use Traversable;
use Zaioll\Shared\Domain\Model\CacheInMemory;
use Zaioll\Shared\Infrastructure\Helper\ArrayHelper;
use Zaioll\Shared\Infrastructure\Helper\StringHelper;
use Zaioll\Shared\Infrastructure\Component\Redis\RedisClientAdapter;

class RedisSimpleCache implements CacheInMemory
{
    protected const PREFIX_KEY = '';

    private $redis;

    public function __construct(RedisClientAdapter $redis)
    {
        $this->redis = $redis;
    }

    /**
     * @inheritDoc
     */
    public function prefixed(string $key): string
    {
        if (StringHelper::byteLength(static::PREFIX_KEY) > 0) {
            return static::PREFIX_KEY . ".{$key}";
        }
        return $key;
    }

    /**
     * @inheritDoc
     */
    public function get(string $key, mixed $default = null): mixed
    {
        $value = $this->redis->get($key);
        if ($value === false) {
            return $default;
        }
        return $value;
    }

    /**
     * @inheritDoc
     */
    public function getMultiple(iterable $keys, mixed $default = null): iterable
    {
        if (! $keys instanceof Traversable || ! is_array($keys)) {
            throw new \Psr\SimpleCache\InvalidArgumentException('');
        }
        $values = $this->redis->mGet($keys);
        foreach ($values as &$value) {
            if ($value === 'FALSE') {
                $value = $default;
            }
        }
        return $values;
    }

    /**
     * @inheritDoc
     */
    public function set(string $key, mixed $value, null|int|\DateInterval $ttl = null): bool
    {
        return $this->redis->set($key, $value, $ttl);
    }

    /**
     * @inheritDoc
     */
    public function setMultiple(iterable $values, null|int|\DateInterval $ttl = null): bool
    {
        if ($ttl !== null && ArrayHelper::count($values) > 0) {
            foreach ($values as $key => $value) {
                $this->redis->set($key, $value, $ttl);
            } 
        }
        return $this->redis->mSet($values);
    }

    /**
     * @inheritDoc
     */
    public function delete(string $key): bool
    {
        if (is_string($key)) {
            return $this->redis->del(...[$key]) === 1;
        }
        return false;
    }

    /**
     * @inheritDoc
     */
    public function deleteMultiple(iterable $keys): bool
    {
        if (ArrayHelper::count($keys) > 0) {
            return $this->redis->del($keys);
        }
        return $this->delete($keys);
    }

    /**
     * @inheritDoc
     */
    public function has(string $key): bool
    {
        return $this->redis->exists($key) > 0;
    }

    /**
     * @inheritDoc
     */
    public function clear(): bool
    {
        return $this->redis->del([$this->prefixed('*')]) > 0;
    }

    /**
     * @inheritDoc
     */
    public function getAll()
    {
        return $this->redis->scan($this->prefixed('*'));
    }
}

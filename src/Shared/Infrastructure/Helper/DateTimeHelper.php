<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Helper;

use DateTime;
use DateTimeImmutable;
use DateTimeZone;
use Decimal\Decimal;
use Zaioll\Shared\Infrastructure\Helper\StringHelper;

class DateTimeHelper
{
    public const ZONES_X = [ //! retirado da configuração da aplicação legada
        'AC' => 'America/Rio_branco',   'AL' => 'America/Maceio',
        'AP' => 'America/Belem',        'AM' => 'America/Manaus',
        'BA' => 'America/Bahia',        'CE' => 'America/Fortaleza',
        'DF' => 'America/Sao_Paulo',    'ES' => 'America/Sao_Paulo',
        'GO' => 'America/Sao_Paulo',    'MA' => 'America/Fortaleza',
        'MT' => 'America/Cuiaba',       'MS' => 'America/Campo_Grande',
        'MG' => 'America/Sao_Paulo',    'PR' => 'America/Sao_Paulo',
        'PB' => 'America/Fortaleza',    'PA' => 'America/Belem',
        'PE' => 'America/Recife',       'PI' => 'America/Fortaleza',
        'RJ' => 'America/Sao_Paulo',    'RN' => 'America/Fortaleza',
        'RS' => 'America/Sao_Paulo',    'RO' => 'America/Porto_Velho',
        'RR' => 'America/Boa_Vista',    'SC' => 'America/Sao_Paulo',
        'SE' => 'America/Maceio',       'SP' => 'America/Sao_Paulo',
        'TO' => 'America/Araguaia',
    ];

    public const TIMEZONES = [ //! retirado da configuração da aplicação legada
        'AC' => 'America/Rio_branco',   'AL' => 'America/Maceio',
        'AP' => 'America/Belem',        'AM' => 'America/Manaus',
        'BA' => 'America/Bahia',        'CE' => 'America/Fortaleza',
        'DF' => 'America/Bahia',        'ES' => 'America/Bahia',
        'GO' => 'America/Bahia',        'MA' => 'America/Fortaleza',
        'MT' => 'America/Manaus',       'MS' => 'America/Manaus',
        'MG' => 'America/Bahia',        'PR' => 'America/Bahia',
        'PB' => 'America/Fortaleza',    'PA' => 'America/Belem',
        'PE' => 'America/Recife',       'PI' => 'America/Fortaleza',
        'RJ' => 'America/Bahia',        'RN' => 'America/Fortaleza',
        'RS' => 'America/Bahia',        'RO' => 'America/Porto_Velho',
        'RR' => 'America/Boa_Vista',    'SC' => 'America/Bahia',
        'SE' => 'America/Maceio',       'SP' => 'America/Bahia',
        'TO' => 'America/Bahia',
    ];

    /**
     * Interpreta uma string de data e retorna o timestamp UNIX a partir do timezone UTC
     * caso a string não contenha informação de timezone.
     *
     * @param string $time
     *
     * @return int|false
     */
    public static function utcTimeStamp($time)
    {
        $tz = date_default_timezone_get();
        date_default_timezone_set('UTC');
        $timestamp = strtotime(str_replace('/', '-', $time));
        date_default_timezone_set($tz);

        return $timestamp;
    }

    /**
     * Interpreta uma string de data e retorna o timestamp UNIX a partir do timezone local
     * caso a string não contenha informação de timezone.
     *
     * @param string $time
     * @param string $timeZone
     *
     * @return int|false
     */
    public static function localTimeStamp($time, string $timeZone)
    {
        $tz = date_default_timezone_get();
        date_default_timezone_set($timeZone);
        $timestamp = strtotime(str_replace('/', '-', $time));
        date_default_timezone_set($tz);

        return $timestamp;
    }

    /**
     * Método extraído da aplicação legada.
     *
     * @param string $uf
     *
     * @return array<timezone,timezoneString,timezone2>
     */
    public static function getTimezoneConfsFromUf(string $uf)
    {
        if (! isset(static::TIMEZONES[$uf])) {
            return ['', '', ''];
        }
        $now    = self::createDateTimeObjectLocalTimeZone('now', static::TIMEZONES[$uf]);
        $mins   = (new Decimal((int) $now->getOffset()))->div(60);
        $sgn    = ($mins->compareTo(0) === -1 ? -1 : 1);
        $sinal  = ($mins->compareTo(0) === -1 ? '-' : '+');
        $mins   = $mins->abs();
        $hrs    = $mins->div(60)->floor();
        $mins   = $mins->sub($hrs->mul(60));

        $offset = sprintf('%+d:%02d', $hrs->mul($sgn), $mins->toString());

        $timezone = $offset;

        $hora   = str_pad((string) (int) $hrs, 2, '0', STR_PAD_LEFT);
        $minuto = str_pad($mins->toString(), 2, '0', STR_PAD_LEFT);

        $timezone2 = "{$sinal}{$hora}:{$minuto}";

        return [$timezone, static::ZONES_X[$uf], $timezone2];
    }

    /**
     * @param int $timestamp
     *
     * @return DateTime
     */
    public static function createUtcDateTimeObjectFromTimestamp($timestamp): DateTime
    {
        $date = new DateTime('now', new DateTimeZone('UTC'));
        $date->setTimestamp($timestamp);

        return $date;
    }

    /**
     * @return string
     */
    public static function getDefaultTimeZone(): string
    {
        return date_default_timezone_get();
    }

    /**
     * @param int $timestamp
     * @param string $timeZone
     *
     * @return DateTime
     */
    public static function createLocalDateTimeObjectFromTimestamp($timestamp, string $timeZone = '')
    {
        if (StringHelper::byteLength($timeZone) === 0) {
            $timeZone = self::getDefaultTimeZone();
        }
        $date = new DateTime('now', new DateTimeZone($timeZone));
        $date->setTimestamp($timestamp);

        return $date;
    }

    /**
     * Se o timezone não for informado, irá considerar a data de entrada como UTC timezone.
     *
     * @param string $time
     *
     * Uma string data/hora. Valores válidos são explicados em
     * https://www.php.net/manual/en/datetime.formats.php
     *
     * @return DateTime|false UTC timezone
     */
    public static function createDateTimeObjectUtcTimeZone($time)
    {
        $timestamp = self::utcTimeStamp($time);
        if (! $timestamp) {
            return false;
        }

        return self::createUtcDateTimeObjectFromTimestamp($timestamp);
    }

    /**
     * Se o timezone não for informado, irá considerar a data de entrada como Local timezone.
     *
     * @param string $time
     * @param string $timeZone
     *
     * Uma string data/hora. Valores válidos são explicados em
     * https://www.php.net/manual/en/datetime.formats.php
     *
     * @return DateTime|false Local timezone
     */
    public static function createDateTimeObjectLocalTimeZone($time, string $timeZone = '')
    {
        if (StringHelper::byteLength($timeZone) === 0) {
            $timeZone = self::getDefaultTimeZone();
        }
        $timestamp = self::localTimeStamp($time, $timeZone);
        if (! $timestamp) {
            return false;
        }

        return self::createLocalDateTimeObjectFromTimestamp($timestamp, $timeZone);
    }

    /**
     * Cria nova instância de \DateTime com timezone definido na aplicação.
     *
     * @param string $time
     * @param string $timeZone
     *
     * Uma string data/hora. Valores válidos são explicados em
     * https://www.php.net/manual/en/datetime.formats.php
     *
     * @return \DateTime local timezone
     */
    public static function createDateTimeObjectLocalTimeZoneFromUtc($time, string $timeZone = '')
    {
        $timestamp = self::utcTimeStamp($time);
        if (! $timestamp) {
            return false;
        }
        if (StringHelper::byteLength($timeZone) === 0) {
            $timeZone = self::getDefaultTimeZone();
        }
        return self::createLocalDateTimeObjectFromTimestamp($timestamp, $timeZone);
    }

    /**
     * @param string $time
     * @param string $timeZone
     *
     * @return \DateTime Local timezone
     */
    public static function createDateTimeObjectUtcTimeZoneFromLocalTimeZone($time, string $timeZone = '')
    {
        if (StringHelper::byteLength($timeZone) === 0) {
            $timeZone = self::getDefaultTimeZone();
        }
        $timestamp = self::localTimeStamp($time, $timeZone);
        if (! $timestamp) {
            return false;
        }

        return self::createUtcDateTimeObjectFromTimestamp($timestamp);
    }

    /**
     * Agora de acordo com timezone UTC
     *
     * @param string $formato
     *
     * @return DateTime|false
     */
    public static function agoraUtc()
    {
        return self::createDateTimeObjectUtcTimeZone('now');
    }

    /**
     * Agora de acordo com o default timezone.
     *
     * @param string $formato
     *
     * @return DateTime|false
     */
    public static function agoraLocal(string $timeZone = '')
    {
        return self::createDateTimeObjectLocalTimeZone('now', $timeZone);
    }

    /**
     * @param string $datetime
     *
     * @return DateTimeImmutable|false
     */
    public static function createDateTimeImmutableUtcTimeZone(string $datetime)
    {
        $date = new DateTimeImmutable($datetime, new DateTimeZone('UTC'));

        return $date;
    }

    /**
     * @param int $seconds
     * @return int
     */
    public static function secondsToMinutes(int $seconds): int
    {
        $min = 0;
        while ($seconds >= 60) {
            $min += 1;
            $seconds -= 60;
        }
        return $min;
    }
}

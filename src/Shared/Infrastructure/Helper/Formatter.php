<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Helper;

use DateTime;
use DateTimeZone;
use Zaioll\Shared\Infrastructure\Helper\DateTimeHelper;

class Formatter
{
    public const DB_DATE_FORMAT       = 'php:Y-m-d';
    public const DB_DATETIME_FORMAT   = 'php:Y-m-d H:i:s';

    /**
     * Formata uma data para php:Y-m-d, timestamp UTC
     *
     * @param \DateTime|string $data
     * @return mixed Data formatada ou valor de entrada.
     */
    public function asDbDate($data)
    {
        if (is_null($data) || empty($data)) {
            return $data;
        }
        if (is_string($data)) {
            $date = DateTimeHelper::createDateTimeObjectUtcTimeZoneFromLocalTimeZone($data);
            return $date->format(substr(self::DB_DATE_FORMAT, 4));
        }
        if ($data instanceof DateTime) {
            $data->setTimezone(new DateTimeZone('UTC'));
            return $data->format(substr(self::DB_DATE_FORMAT, 4));
        }

        return $data;
    }

    /**
     * Formata uma data para php:Y-m-d H:i:s, timestamp UTC
     *
     * @param \DateTime|string $data
     * @return mixed Data formatada ou valor de entrada.
     */
    public function asDbDateTime($data)
    {
        if (is_null($data) || empty($data)) {
            return $data;
        }
        if (is_string($data)) {
            $date = DateTimeHelper::createDateTimeObjectUtcTimeZoneFromLocalTimeZone($data);
            return $date->format(substr(self::DB_DATETIME_FORMAT, 4));
        }
        if ($data instanceof DateTime) {
            $data->setTimezone(new DateTimeZone('UTC'));
            return $data->format(substr(self::DB_DATETIME_FORMAT, 4));
        }

        return $data;
    }

    /**
     * Esse método sempre considerará a data de entrada como estando
     * no timezone UTC.
     *
     * @param string|DateTime $dataHora
     * @param string $timeZone
     *
     * @return string data RFC3338 no timezone local
     */
    public function asLocalDateTimeRFC3339($dataHora, string $timeZone)
    {
        if ($dataHora instanceof DateTime) {
            $dataHora->setTimezone(new DateTimeZone($timeZone));
            return $dataHora->format(DateTime::RFC3339);
        }
        if (is_string($dataHora)) {
            $dateTime = DateTimeHelper::createDateTimeObjectLocalTimeZoneFromUtc($dataHora);
            if ($dateTime) {
                return $dateTime->format(DateTime::RFC3339);
            }
        }
        return $dataHora;
    }

    /**
     * Esse método sempre considerará a data de entrada como estando
     * no timezone local.
     *
     * @param string $dataHora
     * @return string data RFC3339 no timezone UTC
     */
    public function asUtcDateTimeRFC3339($dataHora)
    {
        if ($dataHora instanceof DateTime) {
            $dataHora->setTimezone(new DateTimeZone('UTC'));
            return $dataHora->format(DateTime::RFC3339);
        }

        if (is_string($dataHora)) {
            $dateTime = DateTimeHelper::createDateTimeObjectUtcTimeZoneFromLocalTimeZone($dataHora);
            if ($dateTime) {
                return $dateTime->format(DateTime::RFC3339);
            }
        }

        return $dataHora;
    }
}

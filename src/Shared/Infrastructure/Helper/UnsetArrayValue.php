<?php

namespace Zaioll\Shared\Infrastructure\Helper;

/**
 * Object that represents the removal of array value while performing [[ArrayHelper::merge()]].
 *
 * Usage example:
 *
 * ```php
 * $array1 = [
 *     'ids' => [
 *         1,
 *     ],
 *     'validDomains' => [
 *         'example.com',
 *         'www.example.com',
 *     ],
 * ];
 *
 * $array2 = [
 *     'ids' => [
 *         2,
 *     ],
 *     'validDomains' => new \Zaioll\Shared\Infrastructure\Helper\UnsetArrayValue(),
 * ];
 *
 * $result = \Zaioll\Shared\Infrastructure\Helper\ArrayHelper::merge($array1, $array2);
 * ```
 *
 * The result will be
 *
 * ```php
 * [
 *     'ids' => [
 *         1,
 *         2,
 *     ],
 * ]
 * ```
 */
class UnsetArrayValue
{
    /**
     * Restores class state after using `var_export()`.
     *
     * @param array $state
     * @return UnsetArrayValue
     * @see var_export()
     */
    public static function __set_state($state)
    {
        return new self();
    }
}

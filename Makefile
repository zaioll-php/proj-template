.PHONY :

RED=\033[0;31m
GREEN=\033[0;32m
ORANGE=\033[0;33m
NC=\033[0m

no_cache := no
env := dev
app :=

proj := 
ifeq "$(strip $(proj))" ""
    proj=$(dir)
endif

env_file=.env.${env}
ifeq "$(strip $(env))" "prod"
    env_file=.env
endif


tmp_dockerfile := docker/.Dockerfile
dir := $(shell basename $(CURDIR))
password := $(shell date | sha256sum | base64 | head -c32)

build-microservice-stack: docker/Dockerfile docker

	@printf "\nCompilando imagem para app ${NC}${GREEN}${app}${NC} em ambiente ${NC}${GREEN}'$(env)'${NC}.\n\n"                                                                                               
	@printf "\n${NC}${RED}-----------------------------------------------------------------------------------${RED}${NC}\n"                                                                                   
	@cp docker/Dockerfile ${tmp_dockerfile}
ifeq "$(strip $(no_cache))" "yes"
	docker build -t ${dir}/webstack:${env} -f ${tmp_dockerfile} . --no-cache
else
	docker build -t ${dir}/webstack:${env} -f ${tmp_dockerfile} .
endif
# Undo
	@rm -f ${tmp_dockerfile}
	@printf "\n${NC}${RED}-----------------------------------------------------------------------------------${RED}${NC}\n"                                                                                   
init: docker-compose.yaml docker

	@printf "\n${NC}${RED}-----------------------------------------------------------------------------------${RED}${NC}\n"                                                                                   

	@printf "Global development password: ${password}\n"

	@sed -i -E -e "s/template/${proj}/g;s/password/${password}/g" ./docker-compose.yaml ${env_file}
	@sed -i -E -e "s/php\-proj_template/${proj}/g;" composer.json

	@docker-compose --env-file ${env_file} up -d && docker exec --user developer --workdir /var/www -it ${proj}-webstack composer install
	@git add . && git commit -m "Initial configuration"
	@printf "\n${NC}${GREEN}-----------------------------------------------------------------------------------${RED}${NC}\n"                                                                                   
# Hexagonal architecture based project template

## How to install

### Requirements

- Redis:6.2
- PostgreSQL:14
- Keycloak
- MySQL
- PHP ^8.1
- Composer

```bash
$ composer create-project --prefer-dist zaioll/php-proj_template <proj_name>
```